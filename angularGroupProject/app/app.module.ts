import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DropdownFormComponent } from './dropdown-form/dropdown-form.component';


@NgModule({
  declarations: [
    AppComponent,
    DropdownFormComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
