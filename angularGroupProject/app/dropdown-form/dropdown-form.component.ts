import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown-form',
  templateUrl: './dropdown-form.component.html',
  styleUrls: ['./dropdown-form.component.css']
})

export class DropdownFormComponent {
	dropdownOptions:string[] = ['1 - Best Time','2 - Next Best Time','3 - Not As Good','4 - Worst Time'];
	
  constructor() { }

  onChangeRemoveOption($event, dropdownOptions:string[]){
	let removeListItem:number = $event.target.value;
    let className:string = $event.target.className;
    document.getElementById(className).innerHTML = dropdownOptions[removeListItem];
    dropdownOptions.splice(removeListItem, 1);
  }
  
  onClickSubmit(){
	//console.log('Submit Clicked..');
  }
  
  onClickReset(){
	//consol.log('Reset Clicked..');
  }
}
