import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Dropdown Assignment';
  formTitle = 'Please rate the following times from best(1) to worst(4)';
}
