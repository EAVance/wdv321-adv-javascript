<?php
session_cache_limiter('none');			
session_start();
 
	if ($_SESSION['validUser'] == "yes"){	                    //if valid user or already signed on, skip the rest
		header('Location: finalProjectIndex.php');
	}else{
		if (isset($_POST['submitLogin']) ){			         //if login portion was submitted do the following
		
			$inUsername = $_POST['event_user_name'];	    //pull the username from the form
			$inPassword = $_POST['event_user_password'];	//pull the password from the form
			
			include 'HomeworkPageFiles/connectPDO.php';				        //Connect to the database

			$sql = "SELECT event_user_name,event_user_password FROM event_user WHERE event_user_name = :username AND event_user_password = :password";				
			
			$query = $conn->prepare($sql);                 //prepare the query
			
			$query->bindParam(':username', $inUsername);   //bind parameters to prepared statement
			$query->bindParam(':password', $inPassword);
			
			$query->execute();
			
			$query->fetch();	
			
			if ($query->rowCount() == 1 ){		           //If this is a valid user there should be ONE row only
			
				$_SESSION['validUser'] = "yes";				//this is a valid user so set your SESSION variable
				header('Location: finalProjectIndex.php');
				
			}else{
				
				$_SESSION['validUser'] = "no";				//error in login, not valid user	
				$messageError = "<em style='color:#b20000;'>Incorrect Username or Password. <br>Please try again.</em>";
			}			
			
			$conn = null;
			
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Adv JS Final Project - Beer Rate</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins|Roboto+Condensed:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!--Custom CSS--> 
  <link href="HomeworkPageFiles/finalProjectStyles.css" rel="stylesheet">
  <link href="HomeworkPageFiles/finalProjectLoginStyles.css" rel="stylesheet">
</head>
<body>	
		<h1><img src="HomeworkPageImages/beerLogoSM.png" width="50" height="66" class='logo'/> BEERate</h1>
		<h3><?php echo $messageError;?></h3>
		
		<div id="loginContainer">
			<h2>Admin Login</h2>
            <form method="post" name="loginForm" action="finalProjectLogin.php" >
                <p> 
					<label>Username:</label>
					<input name="event_user_name" type="text" class="form-control"/>
				</p> 
                <p>
					<label>Password:</label>
					<input name="event_user_password" type="password" class="form-control" />
				</p> 
                <p class="formButtons">
					<input name="submitLogin" value="LOGIN" type="submit" class="btn"/> 
					<input name="" type="reset" value="RESET" class="btn"/>&nbsp;
				</p>
            </form>
		</div>         
</body>
</html>