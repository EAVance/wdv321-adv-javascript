<?php
session_start();	

$_SESSION['validUser']='no';  //set validUser to no
session_unset();	//remove all session variables related to current session
session_destroy();	//remove current session

header('Location: finalProjectLogin.php');  //when logout redirect to login page

?>