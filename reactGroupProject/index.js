import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
//import ToDoList, {foo} from './reactGroup/ToDoList.js';
import Survey from './Survey.js';
import registerServiceWorker from './registerServiceWorker';

//ReactDOM.render(<ToDoList />, document.getElementById('root'));
//registerServiceWorker();
ReactDOM.render(<Survey />, document.getElementById('root'));
registerServiceWorker();
